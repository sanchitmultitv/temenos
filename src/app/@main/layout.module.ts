import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutRoutingModule } from './layout-routing.module';
import { LayoutComponent } from './layout.component';
import { MenuComponent } from './menu/menu.component';
import { RouterModule } from '@angular/router';
import { BriefcaseModalComponent } from './briefcase-modal/briefcase-modal.component';
import { LeaderboardModalComponent } from './leaderboard-modal/leaderboard-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppointmentsComponent } from './appointments/appointments.component';
import { QuizModule } from 'src/app/@main/shared/quiz/quiz.module';
import { PdfmodalModule } from 'src/app/@main/pdfmodal/pdfmodal.module';
import { BreakoutComponent } from './breakout/breakout.component';
import { MeetingRoomComponent } from './meeting-room/meeting-room.component';
import { Room2Component } from './room2/room2.component';
import { Room3Component } from './room3/room3.component';
import { Room4Component } from './room4/room4.component';
@NgModule({
  declarations: [LayoutComponent, MenuComponent, BriefcaseModalComponent, LeaderboardModalComponent,AppointmentsComponent, BreakoutComponent, MeetingRoomComponent, Room2Component, Room3Component, Room4Component],
  imports: [
    CommonModule,
    RouterModule,FormsModule,
    LayoutRoutingModule,ReactiveFormsModule,PdfmodalModule,QuizModule
  ]
})
export class LayoutModule { }
