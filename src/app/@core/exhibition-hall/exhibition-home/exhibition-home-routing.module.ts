import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExhibitionHomeComponent } from './exhibition-home.component';

const routes: Routes = [
  {path:'', component:ExhibitionHomeComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExhibitionHomeRoutingModule { }
