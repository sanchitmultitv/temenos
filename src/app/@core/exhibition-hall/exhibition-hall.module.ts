import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExhibitionHallRoutingModule } from './exhibition-hall-routing.module';
import { ExhibitionHallComponent } from './exhibition-hall.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


@NgModule({
  declarations: [ExhibitionHallComponent],
  imports: [
    CommonModule,
    ExhibitionHallRoutingModule,
    BsDatepickerModule.forRoot()
  ]
})
export class ExhibitionHallModule { }
